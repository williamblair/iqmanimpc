CC = psp-g++
INCDIRS = -I$(PSPDEV)/psp/include -I$(PSPDEV)/psp/sdk/include -IGameLib -IGameLib/psp
LIBDIRS = -L$(PSPDEV)/lib -L$(PSPDEV)/psp/lib -L$(PSPDEV)/psp/sdk/lib
LIBS = -lpspdebug -lpspdisplay -lpspge -lpspgum -lpspgu -lpspctrl
CFLAGS = -DPSP_BUILD -O2 -G0
LDFLAGS = -Wl,-zmax-page-size=128

TARGET = IqmAnimPsp

SOURCES = main.cpp $(wildcard GameLib/psp/*.cpp)

all:
	$(CC) $(INCDIRS) $(CFLAGS) $(LIBDIRS) $(LDFLAGS) $(SOURCES) -o $(TARGET) $(LIBS)
	psp-fixup-imports ./$(TARGET)
	mksfoex -d MEMSIZE=1 $(TARGET) PARAM.SFO
	pack-pbp EBOOT.PBP PARAM.SFO NULL NULL NULL NULL NULL $(TARGET) NULL

clean:
	rm -rf $(TARGET) *.elf *.SFO EBOOT.PBP
