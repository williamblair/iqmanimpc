#version 330 core

// texture coordinate from vertex shader
in vec2 texCoord;

// output fragment color
out vec4 FragColor;

uniform sampler2D uTexture0;

void main()
{
    FragColor = texture(uTexture0, texCoord);
}
