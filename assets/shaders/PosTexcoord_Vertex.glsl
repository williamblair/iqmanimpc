#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

// send the given texture coord to the fragment shader
out vec2 texCoord;

// the model-view-projection matrix
uniform mat4 uMvpMatrix;

void main()
{
    gl_Position = uMvpMatrix * vec4(aPos, 1.0);
    texCoord = aTexCoord;
}
