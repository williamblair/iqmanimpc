#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

// send the given color to the fragment shader
out vec4 fragColor;

// the model-view-projection matrix
uniform mat4 uMvpMatrix;

void main()
{
    gl_Position = uMvpMatrix * vec4(aPos, 1.0);
    fragColor   = vec4(aColor, 1.0);
}
