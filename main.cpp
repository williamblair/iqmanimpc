#include <GameMath/GameMath.h>
#include <Renderer.h>
#include <Input.h>
#include <Camera.h>
#include <Texture.h>
#include <IqmMesh.h>
#include <TileFloor.h>
#include <GameTimer.h>
#include <ThirdPersonPlayer.h>

#ifdef PC_BUILD
using namespace GameLib::PC;
#undef main
#endif

#ifdef WII_BUILD
// IQM texture
#include "Gina.h"
#include "Gina_tpl.h"
// Ground texture
#include "grass.h"
#include "grass_tpl.h"
using namespace GameLib::Wii;
#endif

#ifdef PS3_BUILD
using namespace GameLib::PS3;
#endif

#ifdef PSP_BUILD
PSP_MODULE_INFO("IqmAnimPsp", 0, 1, 0);
using namespace GameLib::Psp;
#endif


static Renderer gRender;
static Input gInput;
static VertexBuffer gTriBuf;
static Texture gGrassTex;
static Texture gIqmTex;
static IqmMesh gIqm;
static TileFloor gFloor;
static GameTimer gTimer;
static ThirdPersonPlayer gPlayer;

static bool InitTriBuf()
{
    float vertices[5*3] = 
    {
        // position             texc oord
        -0.5f, -0.5f, 0.0f,     0.0f, 0.0f,
        0.5f, -0.5f, 0.0f,      1.0f, 0.0f,
        0.0f, 0.5f, 0.0f,       0.5f, 1.0f
    };
    int indices[3] = {
        0, 1, 2
    };
    return gTriBuf.Init(
        VertexBuffer::POS_TEXCOORD,
        vertices, 5*3,
        indices, 3
    );
}

static void MovePlayer(const float dt)
{    
    bool moved = false;
    const float moveSpeed = 5.0f;
    if (gInput.Left()) {
        gPlayer.Move(1.0f, 0.0f, moveSpeed * dt);
        moved = true;
    } else if (gInput.Right()) {
        gPlayer.Move(-1.0f, 0.0f, moveSpeed * dt);
        moved = true;
    }
    if (gInput.Up()) {
        gPlayer.Move(0.0f, -1.0f, moveSpeed * dt);
        moved = true;
    } else if (gInput.Down()) {
        gPlayer.Move(0.0f, 1.0f, moveSpeed * dt);
        moved = true;
    }
    
    const float rotSpeed = 5.0f;
    if (gInput.MouseMoveX() != 0) {
        gPlayer.GetCamera().AddYaw((float)gInput.MouseMoveX() * -rotSpeed * dt);
    }
    if (gInput.MouseMoveY() != 0) {
        gPlayer.GetCamera().AddPitch((float)gInput.MouseMoveY() * -rotSpeed * dt);
    }

#ifdef PSP_BUILD
    if (gInput.LTrigger()) {
        gPlayer.GetCamera().AddYaw(-rotSpeed * dt * 5.0f);
    }
    if (gInput.RTrigger()) {
        gPlayer.GetCamera().AddYaw(rotSpeed * dt * 5.0f);
    }
#endif
    
    if (!moved) {
        gPlayer.Move(0.0f, 0.0f, moveSpeed * dt); // force idle anim by default
    }
}

int main(int argc, char* argv[])
{
#ifdef PC_BUILD
    if (!gRender.Init(800, 600, "Hello World")) { return 1; }
    if (!gInput.Init()) { return 1; }
    if (!gGrassTex.LoadFromTGA("assets/grass.tga")) { return 1; }
    if (!gIqm.Init("assets/gina.iqm")) { return 1; }
    if (!gIqmTex.LoadFromTGA("assets/gina.tga")) { return 1; }
#endif
#ifdef WII_BUILD
    if (!gRender.Init(640, 480, "Hello World")) { return 1; }
    if (!gInput.Init()) { return 1; }
    if (!gGrassTex.Init(grass_tpl, grass_tpl_size, grass)) { return 1; }
    if (!gIqm.Init("sd:/gina.iqm")) { return 1; }
    if (!gIqmTex.Init(Gina_tpl, Gina_tpl_size, gina)) { return 1; }
#endif
#ifdef PS3_BUILD
    if (!gRender.Init(800, 600, "Hello World")) { return 1; }
    if (!gInput.Init()) { return 1; }
    if (!gGrassTex.LoadFromTGA("/dev_hdd0/iqmanimpc/grass.tga")) { return 1; }
    if (!gIqm.Init("/dev_hdd0/iqmanimpc/gina.iqm")) { return 1; }
    if (!gIqmTex.LoadFromTGA("/dev_hdd0/iqmanimpc/gina.tga")) { return 1; }
#endif
#ifdef PSP_BUILD
    if (!gRender.Init(800, 600, "Hello World")) { return 1; }
    if (!gInput.Init()) { return 1; }
    if (!gGrassTex.LoadFromTGA("ms0:/grass.tga")) { return 1; }
    if (!gIqm.Init("ms0:/gina.iqm")) { return 1; }
    if (!gIqmTex.LoadFromTGA("ms0:/gina.tga")) { return 1; }
#endif
    if (!InitTriBuf()) { return 1; }
    {
        IqmMesh::Anim idleAnim = { 0, 180, "idle" };
        IqmMesh::Anim walkAnim = { 181, 181+30-1, "walk" };
        gIqm.AddAnim(idleAnim);
        gIqm.AddAnim(walkAnim);
        gIqm.SetAnim("idle");
    }
    if (!gFloor.Init()) { return 1; }
    gFloor.SetScale(5.0f);
    gFloor.SetTexture(&gGrassTex);

    gPlayer.SetModel(&gIqm);
    gPlayer.SetModelScale(GameMath::Vec3(0.1f, 0.1f, 0.1f));
    gPlayer.SetTexture(&gIqmTex);
    gPlayer.GetCamera().SetDistance(15.0f);
    gPlayer.SetDrawYawOffset(180.0f);
    gPlayer.SetDrawPitchOffset(-90.0f);
    gPlayer.Update(0.0f);
    gInput.Update();
    
    gTimer.Update();
    while (!gInput.Quit())
    {
        float dt = gTimer.Update();
        gInput.Update();
        
        MovePlayer(dt);
        gPlayer.Update(dt);
        
        gRender.Clear();
        
        gPlayer.Draw(gRender);
        gRender.DrawAabb(gPlayer.GetAabb(), gPlayer.GetViewMat());
        gFloor.Draw(gPlayer.GetViewMat(), gRender);

        gRender.Update();
    }

    return 0;
}

