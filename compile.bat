:: use with Visual Studio Developer Command Prompt For VS 2019
::set VSLIBSDIR=D:\vs_libs
set VSLIBSDIR=C:\VSLibs
set INCDIRS=/I %VSLIBSDIR%\include /I ./ /I GameLib\pc /I GameLib
set LIBDIRS=/link /LIBPATH:%VSLIBSDIR%\lib
set LIBS=glew32.lib opengl32.lib SDL2.lib SDL2main.lib SDL2_mixer.lib
set CFLAGS=/EHsc /O2 /D PC_BUILD
:: TODO - how do I get this on multiple lines...
set SOURCES=main.cpp GameLib\pc\Renderer.cpp GameLib\pc\Input.cpp GameLib\pc\VertexBuffer.cpp GameLib\pc\Shader.cpp GameLib\pc\Camera.cpp GameLib\pc\MoveComponent.cpp GameLib\pc\Texture.cpp GameLib\pc\IqmMesh.cpp GameLib\pc\TileFloor.cpp GameLib\pc\GameTimer.cpp GameLib\pc\ThirdPersonPlayer.cpp GameLib\pc\Music.cpp GameLib\pc\SoundEffect.cpp GameLib\pc\Sprite.cpp GameLib\pc\Thread.cpp


cl %CFLAGS% %SOURCES% %INCDIRS% %LIBDIRS% %LIBS%

